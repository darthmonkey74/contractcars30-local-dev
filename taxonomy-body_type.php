<?php
/**
* The template for displaying Archive pages.
*
* Learn more: http://codex.wordpress.org/Template_Hierarchy
*
* @package progression
*/

get_header('cars'); ?>
<?php get_template_part( 'page', 'vehicle-title' ); ?>
<div class="width-container">

<?php if ( have_posts() ) : ?>

<div id="page-title">

<h1 id="page-heading">
<?php if (is_tax('vehicle_type')) {
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$tax_term_breadcrumb_taxonomy_slug = $term->taxonomy;
echo '' . $term->name . '';
}
?>
</h1>
</div><!-- close #page-title -->

<div id="4-content-container" class="tax-vehicle-container">
<?php echo do_shortcode('[vehicle_searchform include="make,model"]') ?>  
<?php
/* ADDED by Roshan for sorting ASCENDING order but call for price last */
function my_sort_custom( $orderby, $query ){
    global $wpdb;
    $orderby =  "   case when CAST(meta_value AS SIGNED) = '0' then 1 else 0 end ,CAST(meta_value AS SIGNED)   ";
	return  $orderby;
}
add_filter('posts_orderby','my_sort_custom',10,2);
global $wp_query;
$args = array_merge( $wp_query->query_vars, array('meta_key' => 'price'));
//$args = array_merge( $wp_query->query_vars, array( 'orderby' => 'meta_value_num','meta_key' => 'price','order' => 'ASC'));
query_posts( $args );
?>

<?php
/* Start the Loop */
$count = 1;
$count_2 = 1;
?>

<?php while ( have_posts() ) : the_post();
if($count >= 5) { $count = 1; }
?>

<div class="grid4column-progression <?php if($count == 4): echo ' lastcolumn-progression'; endif; ?>">
<?php
get_template_part( 'content', 'vehicle');
?>

</div>
<?php if($count == 4): ?><div class="clearfix"></div><?php endif; ?>
<?php $count ++; $count_2++; endwhile; ?>

<div class="clearfix"></div>
<?php show_pagination_links( ); ?>

</div><!-- close #content-container -->

<?php else : ?>

<?php get_template_part( 'no-results', 'inventory' ); ?>

</div><!-- close #content-container -->
<?php endif; ?>

<?php get_sidebar( 'vehicle' ); ?>
</div><!-- close .width-container -->
<?php get_footer(); ?>