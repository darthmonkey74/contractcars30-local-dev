<?php
/**
 * @package progression
 */
?>

<div class="content-container-boxed vehicle-widget">
	
	<a href="<?php the_permalink(); ?>">
		<?php if(has_post_thumbnail()): ?>
			<div class="vehicle-index-widget">
				<?php the_post_thumbnail('vehicle-index'); ?>
			</div>
		<?php else: ?>
		<div class="blank-image-index-vehicle"></div>
		<?php endif; ?>
		
        <!-- Advanced custom field & corner flash HTML-->
       <!-- <div class="corner_flash_1"><?php //the_field('sales_flash'); ?></div>--><!-- Ashish Comment--->
	   
	   <div class="corner_flash_1">
	     <?php //the_field('sales_flash'); ?>
	   <?php 
	   /*if(the_field('sales_flash') == "None"){
	      echo '<img src="'.get_template_directory_uri().'/images/FLASH-special-offer.png">';
	   } else if(the_field('sales_flash') == "Limited Stock"){
	      echo '<img src="'.get_template_directory_uri().'/images/FLASH-special-offer.png">';
	   } else if(the_field('sales_flash') == "Special Deal"){
	      echo '<img src="'.get_template_directory_uri().'/images/FLASH-special-offer.png">';
	   } else if(the_field('sales_flash') == "Special Offer"){
	      echo '<img src="'.get_template_directory_uri().'/images/FLASH-special-offer.png">';
	   }*/
	   
	   switch (get_field('sales_flash')) {
	   case "None":
          echo '';
         break;
     case "Limited Stock":
          echo '<img src="'.get_template_directory_uri().'/images/FLASH-limited-stock.png">';
         break;
     case "Special Deal":
          echo '<img src="'.get_template_directory_uri().'/images/FLASH-special-deal.png">';
         break;
     case "Special Offer":
         echo '<img src="'.get_template_directory_uri().'/images/FLASH-special-offer.png">';
         break;
}
	   ?>
	   </div>
        <!-- END Advanced custom field & corner flash HTML-->
		<h3 class="vehicle-widget-title"><?php the_title(); ?></h3>
	</a>
	
	<div id="vehicle-price-index">
		<?php echo do_shortcode('[vehicle_price]'); ?>
        <p>
		<a href="<?php the_permalink(); ?>#alternative-payment-profiles" class="progression-button button-vehicle-index"><?php _e( 'View<br/>Offer', 'progression' ); ?></a>
        <a href="<?php the_permalink(); ?>#progression_contact" class="progression-button button-vehicle-index green"><?php _e( 'Contact<br/>Us', 'progression' ); ?></a>
        </p>
		<div class="clearfix"></div>
	</div>
	
	<div class="clearfix"></div>
</div><!-- close .content-container-boxed -->