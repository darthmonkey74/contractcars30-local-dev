<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package progression
 * @since progression 1.0
 */
?>
<div id="sidebar">
	<div id="price-sidebar" class="sidebar-item widget widget_text">
		<h5 class="widget-title"><?php _e( 'Vehicle Price', 'progression' ); ?></h5>			
		<div id="sidebar-price"><?php echo do_shortcode('[vehicle_price]'); ?></div>
        <h6>Payment Profile: <?php the_field('payment_profile'); ?></h6><!-- Advanced custom field - payemt profile -->
		<?php if ( is_active_sidebar( 'vehicle-contact' ) ) : ?>
			<div class="sidebar-button-price"><a class="progression-button" id="button-select-progression"><?php _e( 'Request Information', 'progression' ); ?></a></div>
		<?php endif; ?>
        <div class="clearfix"></div>
	</div>
	
	<div class="sidebar-divider"></div>
	
	<?php if ( ! dynamic_sidebar( 'sidebar-vehicle-single' ) ) : ?>
	<?php endif; // end sidebar widget area ?>
	
</div><!-- close #sidebar -->