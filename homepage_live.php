<?php
// Template Name: HomePage
/**
 *
 * @package progression
 * @since progression 1.0
 */

get_header(); ?>
	
<?php // get_template_part( 'slider', 'progression' ); ?>
<div class="width-container content_width-container">
    <!-- Left hand side bar -->
	<?php if ( is_active_sidebar( 'homepage_left' ) ) : ?>
	<div id="secondary" class="sidebar_left" role="complementary">
	<?php dynamic_sidebar( 'homepage_left' ); ?>
	</div>
	<?php endif; ?>
        <div class="middle_content">

        <?php while(have_posts()): the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
        <?php echo do_shortcode('[vehicle_searchform include="make,model"]') ?>  
        <!-- this code pull in the homepage slider -->        
        <?php echo do_shortcode('[rev_slider homepage]') ?>  

		
		<!--
		<div id="amazingcarousel-container-1">
    <div id="amazingcarousel-1" style="display:block;position:relative;width:100%;max-width:720px;margin:0px auto 0px;">
        <div class="amazingcarousel-list-container">
            <ul class="amazingcarousel-list">
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/test/Chrysanthemum-lightbox.jpg" title="Chrysanthemum"  class="html5lightbox" data-group="amazingcarousel-1"><img src="<?php bloginfo('template_url'); ?>/images/test/Chrysanthemum.jpg"  alt="Chrysanthemum" /></a></div>
<div class="amazingcarousel-title">Chrysanthemum</div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/test/Desert-lightbox.jpg" title="Desert"  class="html5lightbox" data-group="amazingcarousel-1"><img src="<?php bloginfo('template_url'); ?>/images/test/Desert.jpg"  alt="Desert" /></a></div>
<div class="amazingcarousel-title">Desert</div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/test/Hydrangeas-lightbox.jpg" title="Hydrangeas"  class="html5lightbox" data-group="amazingcarousel-1"><img src="<?php bloginfo('template_url'); ?>/images/test/Hydrangeas.jpg"  alt="Hydrangeas" /></a></div>
<div class="amazingcarousel-title">Hydrangeas</div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/test/Jellyfish-lightbox.jpg" title="Jellyfish"  class="html5lightbox" data-group="amazingcarousel-1"><img src="<?php bloginfo('template_url'); ?>/images/test/Jellyfish.jpg"  alt="Jellyfish" /></a></div>
<div class="amazingcarousel-title">Jellyfish</div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/test/Koala-lightbox.jpg" title="Koala"  class="html5lightbox" data-group="amazingcarousel-1"><img src="<?php bloginfo('template_url'); ?>/images/test/Koala.jpg"  alt="Koala" /></a></div>
<div class="amazingcarousel-title">Koala</div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/test/Lighthouse-lightbox.jpg" title="Lighthouse"  class="html5lightbox" data-group="amazingcarousel-1"><img src="<?php bloginfo('template_url'); ?>/images/test/Lighthouse.jpg"  alt="Lighthouse" /></a></div>
<div class="amazingcarousel-title">Lighthouse</div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/test/Penguins-lightbox.jpg" title="Penguins"  class="html5lightbox" data-group="amazingcarousel-1"><img src="<?php bloginfo('template_url'); ?>/images/test/Penguins.jpg"  alt="Penguins" /></a></div>
<div class="amazingcarousel-title">Penguins</div>                    </div>
                </li>
                <li class="amazingcarousel-item">
                    <div class="amazingcarousel-item-container">
<div class="amazingcarousel-image"><a href="images/test/Tulips-lightbox.jpg" title="Tulips"  class="html5lightbox" data-group="amazingcarousel-1"><img src="<?php bloginfo('template_url'); ?>/images/test/Tulips.jpg"  alt="Tulips" /></a></div>
<div class="amazingcarousel-title">Tulips</div>                    </div>
                </li>
            </ul>
            <div class="amazingcarousel-prev"></div>
            <div class="amazingcarousel-next"></div>
        </div>
        <div class="amazingcarousel-nav"></div>
        
    </div>
</div>
<!-- End of body section HTML codes -->


        <!-- display homepage widgets -->
        <?php if ( is_active_sidebar( 'homepage-widgets' ) ) : ?>
            <?php dynamic_sidebar( 'homepage-widgets' ); ?>
        <?php endif; ?>
        
        <?php wp_reset_query(); ?>
        <!-- Homepage Child Pages Start -->
        <?php
        $args = array(
            'post_type' => 'page',
            'numberposts' => -1,
            'post' => null,
            'post_parent' => $post->ID,
            'order' => 'ASC',
            'orderby' => 'menu_order'
        );
        $features = get_posts($args);
        $features_count = count($features);
        if($features):
            $count = 1;
            foreach($features as $post): setup_postdata($post);
                $image_id = get_post_thumbnail_id();
                $image_url = wp_get_attachment_image_src($image_id, 'large');
                $col_count_progression = get_theme_mod('home_col_progression', '3');
                if($count >= 1+$col_count_progression) { $count = 1; }
        ?>
            <div class="home-child-boxes grid<?php echo get_theme_mod('home_col_progression', '3'); ?>column-progression <?php if($count == get_theme_mod('home_col_progression', '3')): echo ' lastcolumn-progression'; endif; ?>">
                <?php if(get_post_meta($post->ID, 'pageoptions_home-box-link', true)): ?><a href="<?php echo get_post_meta($post->ID, 'pageoptions_home-box-link', true) ?>"><?php endif; ?>
                <div class="home-child-boxes-container">
                    <?php if($image_url[0]): ?><div class="childpage-image"><img src="<?php echo $image_url[0]; ?>" alt="<?php the_title(); ?>"></div><?php endif; ?>
    
                    <h4 class="home-child-title"><?php the_title(); ?></h4>
                    <?php the_content(); ?>
                </div>
                <?php if(get_post_meta($post->ID, 'pageoptions_home-box-link', true)): ?></a><?php endif; ?>
            </div>
        <?php if($count == get_theme_mod('home_col_progression', '3')): ?><div class="clearfix"></div><?php endif; ?>
        <?php $count ++; endforeach; ?>
        <?php endif; ?>
    </div><!-- End middle content -->
     <!-- right hand side bar -->
     <?php if ( is_active_sidebar( 'homepage_right' ) ) : ?>
	<div id="tertiary" class="sidebar_right" role="complementary">
	<?php dynamic_sidebar( 'homepage_right' ); ?>
	</div>
	<?php endif; ?>
	<!-- Homepage Child Pages End -->
	
	<div class="clearfix"></div>
</div><!-- close .width-container -->
<?php get_footer(); ?>