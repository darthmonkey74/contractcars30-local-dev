<?php
/**
 * The Header for our theme.
 *
 * @package progression
 * @since progression 1.0
 */
?><!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>  <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
	<?php wp_head(); ?>
<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-10100926-29', 'auto');
	  ga('require', 'displayfeatures');
	  ga('send', 'pageview');
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '159743684378969');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=159743684378969&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>

<body <?php body_class('cars_categories'); ?>  onload="_googWcmGet('number', '0161 928 3456')">
<header>
	<div class="width-container">
	<div class="header_left">
       <h1 id="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_theme_mod( 'logo_upload', get_template_directory_uri() . '/images/logo.png' ); ?>" alt="<?php bloginfo('name'); ?>" width="<?php echo get_theme_mod( 'logo_width', '155' ); ?>" /></a></h1>
		<div class="tagline"> <?php echo get_bloginfo ( 'description' );  ?></div>
        <!-- <div id="header-search">
			<?php // if (class_exists('Progression_Car_Dealer')): ?>
			<?php // echo do_shortcode('[vehicle_searchform include="keyword"]'); ?>
			<?php // else: ?>
			<?php // get_search_form(); ?>
			<?php//  endif; ?>
         -->
         </div>
             <div class="header_right">
                 <div id="tel_no">We'll find the best deals for you!<br/>
                     <strong>UK NORTH: <span class="number2">0161 928 3456</span><br>
							 UK SOUTH: <span class="number2">0203 096 3705</span><br/>
                     		 NORTHERN IRELAND: <span class="number2">028 9018 3336</span></strong><br/>
                 </div>
             </div> 
		</div>
		</div>
		<div class="clearfix"></div>
	</div><!-- close .width-container -->
	<nav>
		<div class="width-container">
			<?php wp_nav_menu( array('theme_location' => 'primary', 'depth' => 4, 'menu_class' => 'sf-menu') ); ?>
             <!-- <div class="social-ico">
                    <a target="_blank" href="https://www.facebook.com/contracthire"><i class="fa fa-facebook"></i></a>
                    <a target="_blank" href="https://twitter.com/contractcars"><i class="fa fa-twitter"></i></a>
                    <a target="_blank" href="https://plus.google.com/u/0/+Contractcarsuk/posts"><i class="fa fa-google-plus"></i></a>
                    <a target="_blank" href="https://www.linkedin.com/profile/view?id=141839851&amp;authType=NAME_SEARCH&amp;authToken=uXKl&amp;locale=en_US&amp;trk=tyah2&amp;trkInfo=idx%3A1-1-1%2CtarId%3A1417098147205%2Ctas%3Acontract+cars"><i class="fa fa-linkedin"></i></a>
                 </div> -->
		<div class="clearfix"></div>	
		</div><!-- close .width-container -->
	</nav>
</header>

<div id="main" class="cars_categories">