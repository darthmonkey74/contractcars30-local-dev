<?php
/**
 * Enqueue scripts and styles
 */
function progression_scripts2() {
	wp_enqueue_style( 'progression-style', get_stylesheet_uri(), array(), '8890013', "all");
}
add_action( 'wp_enqueue_scripts', 'progression_scripts2' );

function contact_page_widgets_init() {

	register_sidebar( array(
		'name'          => 'Contact Page sidebar',
		'id'            => 'contact_sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
	    'after_title' => '</h3>',
	) );

}
add_action( 'widgets_init', 'contact_page_widgets_init' );

?>