<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package progression
 * @since progression 1.0
 */
?>
<div id="tertiary" class="sidebar_right" role="complementary">
	<?php if ( ! dynamic_sidebar( 'sidebar-vehicle' ) ) : ?>
	<?php endif; // end sidebar widget area ?>
</div><!-- close #tertiary -->