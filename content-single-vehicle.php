<?php
/**
 * @package progression
 */
?>
<div id="page-title">
		<?php
		
if (function_exists ( 'bcn_display' )) {
			echo '<div id="bread-crumb">';
			bcn_display ();
			echo '</div>';
		}
		?>
		<h1 id="page-heading"><?php the_title(); ?></h1>
</div>
<!-- close #page-title -->

<div id="content-container">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="content-container-boxed">
			
			<?php if (get_field('images') != ''): ?>
				<div class="vehicle-index-gallery">
					<?php echo do_shortcode('[vehicle_gallery]'); ?>
				<div class="clearfix"></div>
			</div>
			<?php else: ?>
			<?php if(has_post_thumbnail()): ?>
				<div class="blog-featured-image">
				<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?>
				<a href="<?php echo $image[0]; ?>" rel="prettyPhoto">
					<?php the_post_thumbnail('vehicle-post-gallery'); ?>
				</a>
			</div>
				<?php else: ?>
				<div class="blank-image-vehicle"></div>
			<?php endif; ?>	
			<?php endif; ?>	
			<div class="clearfix"></div>

			<div id="progression-tab-container" class='progression-tab-container'>
				<ul class='progression-etabs'>
					<!-- <li class='progression-tab'><a href="#progression-description"><?php // _e( 'Description', 'progression' ); ?></a></li> -->
					<!-- <li class='progression-tab'><a href="#progression-specs"><?php _e( 'Specifications', 'progression' ); ?></a></li> Specification tab commented out -->
                <li class='progression-tab'><a href="#alternative-payment-profiles"  >Alternative Payment Profiles</a></li> 				  
				  <?php if ( is_active_sidebar( 'vehicle-contact' ) ) : ?>
					   <li class='progression-tab'><a href="#progression_contact"><?php _e( 'Contact Us', 'progression' ); ?></a></li>
				   <?php endif; ?>
				   
				   
			 	</ul>
				<div class='progression-panel-container'>
					<div id="progression-description">
						<!-- <h3><?php // _e( 'Vehicle Description', 'progression' ); ?></h3> -->
   			   		 	<?php the_content(); ?>
						<?php edit_post_link( __( 'Edit', 'progression' ), '<span class="edit-link">', '</span>' ); ?>
					</div>
						<div id="alternative-payment-profiles">
					
			<!-- payment profile tabels -->
			<div class="pricing tables sidebar-item widget">
				<div class="tablepress_fix" style="color: #fff"><?php
 				   $tablepress_id = get_field( 'custom_table' );
  				  $args = array(
  				    'id'                => $tablepress_id,
  				    'use_datatables'    => true,
    				  'print_name'        => false
  				  );
 				   if ( function_exists( 'tablepress_print_table' ) ) {
 				     tablepress_print_table( $args );
  				  }
				?> </div>
				<table class="myTable">
					<tr>
						<td><strong>Lowest Price</strong></td>
						<td><strong>2 Year Lease</strong></td>
						<td><strong>£500 + 9 + 23</strong></td>
						<td><strong>10k per annum</strong></td>
						<td><strong><?php
						//$price_r = get_field ( 'price', $post_id );
						$testap32_r = get_field ( 'altpaymentprofile32', $post_id );
						$testap44_r = get_field ( 'AltPaymentProfile44', $post_id );
						$price_r_total = 0;
						$price_r_2_6 = 0;
						$price_r_2_3 = 0;
						$price_r_3_9 = 0;
						$price_r_3_6 = 0;
						$price_r_3_3 = 0;
						
						
						if ($testap32_r == 0 || $testap32_r == ""  ) {
							echo "Call for Pricing";
						} else {
							//$price_r_total = $price_r * 32;
							$price_r_total_32 = $testap32_r * 32;
							
							//echo "&pound;" . "" . $price_r;
							echo "&pound;" . "" . $testap32_r;
						}
						
						?></strong></td>
						<?php $testmake =  get_field ( 'make', $post_id ); 
						   $testmakevalue = get_term_by( 'id', $testmake, 'make' );
						
						?>
                        <td><a href="<?php echo site_url(); ?>/quote/?payment-profile=£500plus9plus23&Make=<?php echo $testmakevalue->name; ?>&model-and-variant=<?php the_title(); ?>" class="progression-button button-vehicle-index">Get a quote</a></td>
					</tr>
					<tr class="alt">
						<td>Small Deposit</td>
						<td>2 Year Lease</td>
						<td>£500 + 6 + 23</td>
						<td>10k per annum</td>
						<td>
				<?php
				if ($testap32_r == 0 || $testap32_r == ""  ) {
					echo "Call for Pricing";
				//Below elsif statement is custom
				} elseif (is_single('Mercedes CLS220 BlueTEC AMG Line 5dr Shooting Brake Auto')) {
					$price_r_2_6 = $price_r_total_32 / 29 + 1;
					echo "&pound;" . "" . round ( $price_r_2_6 );
				//Above elsif statement is custom
				} else {
					$price_r_2_6 = $price_r_total_32 / 29;
					echo "&pound;" . "" . round ( $price_r_2_6 );
				}
				
				?>
				</td>
                <td><a href="<?php echo site_url(); ?>/quote/?payment-profile=£500plus6plus23&Make=<?php echo $testmakevalue->name; ?>&model-and-variant=<?php the_title(); ?>"  class="progression-button button-vehicle-index">Get a quote</a></td>
					</tr>
					<tr class="alt">
						<td>Lowest Deposit</td>
						<td>2 Year Lease</td>
						<td>£500 + 3 + 23</td>
						<td>10k per annum</td>
						<td>				<?php
						if ($testap32_r == 0 || $testap32_r == ""  ) {
							echo "Call for Pricing";
						//Below elsif statement is custom
						} elseif (is_single('Mercedes CLS220 BlueTEC AMG Line 5dr Shooting Brake Auto')) {
							$price_r_2_3 = $price_r_total_32 / 26 + 6;
							echo "&pound;" . "" . round ( $price_r_2_3 );
						//Above elsif statement is custom
						} else {
							$price_r_2_3 = $price_r_total_32 / 26;
							echo "&pound;" . "" . round ( $price_r_2_3 );
						}
						
						?></td>
                        <td><a href="<?php echo site_url(); ?>/quote/?payment-profile=£500plus3plus23&Make=<?php echo $testmakevalue->name; ?>&model-and-variant=<?php the_title(); ?>"  class="progression-button button-vehicle-index">Get a quote</a></td>
					</tr>
				</table>

				<table class="myTable">
					<tr>
						<td><strong>Lowest Price</strong></td>
						<td><strong>3 Year Lease</strong></td>
						<td><strong>£500 + 9 + 35</strong></td>
						<td><strong>10k per annum</strong></td>
						<td><strong>				<?php
						if ($testap44_r == 0 || $testap44_r =="" ) {
							echo "Call for Pricing";
						} else {
						    $price_r_total_44 = $testap44_r * 44;
							//$price_r_3_9 = $price_r_total / 44;
							echo "&pound;" . "" . round ( $testap44_r );
						}
						
						?></strong></td>
                        <td><a href="<?php echo site_url(); ?>/quote/?payment-profile=£500plus9plus35&Make=<?php echo $testmakevalue->name; ?>&model-and-variant=<?php the_title(); ?>"  class="progression-button button-vehicle-index">Get a quote</a></td>
					</tr>
					<tr class="alt">
						<td>Small Deposit</td>
						<td>3 Year Lease</td>
						<td>£500 + 6 + 35</td>
						<td>10k per annum</td>
						<td>				<?php
						if ($testap44_r == 0 || $testap44_r =="" ) {
							echo "Call for Pricing";
						//Below elsif statement is custom
						} elseif (is_single('Mercedes CLS220 BLUETEC AMG Line 4DR Coupe Auto')) {
							$price_r_3_6 = $price_r_total_44 / 41 + 4;
							echo "&pound;" . "" . round ( $price_r_3_6 );
						//Above elsif statement is custom
						} else {
							$price_r_3_6 = $price_r_total_44 / 41;
							echo "&pound;" . "" . round ( $price_r_3_6 );
						}
						
						?></td>
                        <td><a href="<?php echo site_url(); ?>/quote/?payment-profile=£500plus6plus35&Make=<?php echo $testmakevalue->name; ?>&model-and-variant=<?php the_title(); ?>"  class="progression-button button-vehicle-index">Get a quote</a></td>
					</tr>
					<tr class="alt">
						<td>Lowest Deposit</td>
						<td>3 Year Lease</td>
						<td>£500 + 3 + 35</td>
						<td>10k per annum</td>
						<td>				<?php
						if ($testap44_r == 0 || $testap44_r =="" ) {
							echo "Call for Pricing";
						//Below elsif statement is custom
						} elseif (is_single('Mercedes CLS220 BLUETEC AMG Line 4DR Coupe Auto')) {
							$price_r_3_3 = $price_r_total_44 / 38 + 8;
							echo "&pound;" . "" . round ( $price_r_3_3 );
						} elseif (is_single('Mercedes CLS220 BlueTEC AMG Line 5dr Shooting Brake Auto')) {
							$price_r_3_3 = $price_r_total_44 / 38 + 5;
							echo "&pound;" . "" . round ( $price_r_3_3 );
						//Above elsif statement is custom
						} else {
							$price_r_3_3 = $price_r_total_44 / 38;
							echo "&pound;" . "" . round ( $price_r_3_3 );
						}
						
						?></td>
                        <td><a href="<?php echo site_url(); ?>/quote/?payment-profile=£500plus3plus35&Make=<?php echo $testmakevalue->name; ?>&model-and-variant=<?php the_title(); ?>"  class="progression-button button-vehicle-index">Get a quote</a></td>
					</tr>
				</table>
                       
			</div>
			<!-- END payment profile tabels -->
					</div>
					<!-- close #progression-description -->
					<!-- <div id="progression-specs">
						<h3><?php _e( 'Vehicle Specifications', 'progression' ); ?></h3>
						<?php echo do_shortcode('[vehicle_specs]'); ?>
			  	  	</div> -->
					<!-- close #progression-specs commented out  -->
					<?php if ( is_active_sidebar( 'vehicle-contact' ) ) : ?>
						<div id="progression_contact">
							<?php if ( ! dynamic_sidebar( 'vehicle-contact' ) ) : ?>
							<?php endif; // end sidebar widget area ?>
						</div>
					<!-- close #progression_contact -->
					<?php endif; ?>
					
					
					
					
					
				</div>
				<!-- close progresion-panel-container -->
			</div>
			<!-- close progressoinn-tab-container -->


			<div class="clearfix"></div>
		</div>
		<!-- close .content-container-boxed -->
	</article>
	<!-- #post-## -->
</div>
<!-- close #content-container -->
<!-- Open google analytics -->
<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-10100926-29', 'auto');
	  ga('require', 'displayfeatures');
	  ga('send', 'pageview');
</script>
<!-- close analytics -->