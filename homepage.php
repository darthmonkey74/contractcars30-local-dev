<?php
// Template Name: HomePage
/**
 *
 * @package progression
 * @since progression 1.0
 */

get_header(); ?>
<div class="width-container content_width-container">
    <!-- Left hand side bar -->
	<?php if ( is_active_sidebar( 'homepage_left' ) ) : ?>
	<div id="secondary" class="sidebar_left" role="complementary">
	<?php dynamic_sidebar( 'homepage_left' ); ?>
	</div>
	<?php endif; ?>
        <div class="middle_content">

        <?php while(have_posts()): the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
        <?php echo do_shortcode('[vehicle_searchform include="make,model"]') ?>  
		
<?php
add_action('widgets_init', 'pyre_homepage_vehicle_load_widgets');
		global $post;
		
		extract($args);
		
		$title = apply_filters('widget_title', $instance['title']);
		$posts = $instance['posts'];
		//$columns = $instance['columns'];
		$columns = 3;
	
		
		
		$recent_posts = new WP_Query(array(
			'post_type'           => 'vehicle',
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'posts_per_page'      => $posts,
			'orderby'             => 'meta_value_num',
			'meta_key'            => 'price',
		'tax_query' => array(
				array(
					'taxonomy' => 'special_offers',
					'field'    => 'slug',
					'terms'    => 'home-page-offers',
				),
			),
			'order'               => 'ASC'
		));
		
		// Get a new second collection to show after the "top six"
		$slider_specials = new WP_Query(array(
			'post_type'           => 'vehicle',
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'posts_per_page'      => $posts,
			'orderby'             => 'meta_value_num',
			'meta_key'            => 'price',
			'tax_query' => array(
				array(
					'taxonomy' => 'special_offers',
					'field' => 'slug',
					'terms' => 'specials-for-the-slider', // Disabled for now because the slider is overly full.
				)
			),
			'order'               => 'ASC'
		));
		
		if($recent_posts->have_posts()):
			$count = 1;
			$count_2 = 1;
		?>
		<div class="slider5">
		<?php while($recent_posts->have_posts()): $recent_posts->the_post(); 
	     ?>
	     
         <div class="slide">
	    <?php get_template_part( 'content', 'vehicle-widget-slide'); ?>
		 </div>
		<?php 
		 endwhile;
		 
		 endif;
		 
		// This will iterate over the new collection
		if ($slider_specials->have_posts()):
			while($slider_specials->have_posts()): $slider_specials->the_post(); ?>
				<div class="slide">
					<?php get_template_part( 'content', 'vehicle-widget-slide'); ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
		</div>

		
		
        <!-- display homepage widgets -->
        <?php if ( is_active_sidebar( 'homepage-widgets' ) ) : ?>
            <?php dynamic_sidebar( 'homepage-widgets' ); ?>
        <?php endif; ?>
        
        <?php wp_reset_query(); ?>
        <!-- Homepage Child Pages Start -->
        <?php
        $args = array(
            'post_type' => 'page',
            'numberposts' => -1,
            'post' => null,
            'post_parent' => $post->ID,
            'order' => 'ASC',
            'orderby' => 'menu_order'
        );
        $features = get_posts($args);
        $features_count = count($features);
        if($features):
            $count = 1;
            foreach($features as $post): setup_postdata($post);
                $image_id = get_post_thumbnail_id();
                $image_url = wp_get_attachment_image_src($image_id, 'large');
                $col_count_progression = get_theme_mod('home_col_progression', '3');
                if($count >= 1+$col_count_progression) { $count = 1; }
        ?>
            <div class="home-child-boxes grid<?php echo get_theme_mod('home_col_progression', '3'); ?>column-progression <?php if($count == get_theme_mod('home_col_progression', '3')): echo ' lastcolumn-progression'; endif; ?>">
                <?php if(get_post_meta($post->ID, 'pageoptions_home-box-link', true)): ?><a href="<?php echo get_post_meta($post->ID, 'pageoptions_home-box-link', true) ?>"><?php endif; ?>
                <div class="home-child-boxes-container">
                
    
                    <h4 class="home-child-title"><?php the_title(); ?></h4>
                    <?php the_content(); ?>
                </div>
                <?php if(get_post_meta($post->ID, 'pageoptions_home-box-link', true)): ?></a><?php endif; ?>
            </div>
        <?php if($count == get_theme_mod('home_col_progression', '3')): ?><div class="clearfix"></div><?php endif; ?>
        <?php $count ++; endforeach; ?>
        <?php endif; ?>
    </div><!-- End middle content -->
     <!-- right hand side bar -->
     <?php if ( is_active_sidebar( 'homepage_right' ) ) : ?>
	<div id="tertiary" class="sidebar_right" role="complementary">
	<?php dynamic_sidebar( 'homepage_right' ); ?>
	</div>
	<?php endif; ?>
	<!-- Homepage Child Pages End -->
	
	<div class="clearfix"></div>
</div><!-- close .width-container -->
<?php get_footer(); ?>