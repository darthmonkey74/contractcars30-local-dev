<?php
// Template Name: Contact Page
/**
 *
 * @package progression
 * @since progression 1.0
 */

get_header(); ?>
<?php get_template_part( 'page', 'title' ); ?>
<style>

#bread-crumb {
	display: none;
}
</style>
<div class="width-container">
	
	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'content', 'page' ); ?>
	<?php endwhile; // end of the loop. ?>
	
<?php if ( is_active_sidebar( 'contact_sidebar' ) ) : ?>
	<div id="secondary" class="widget-area sidebar_right contact-sidebar" role="complementary">
	<?php dynamic_sidebar( 'contact_sidebar' ); ?>
	</div>
<?php endif; ?>
	
</div><!-- close .width-container -->
<?php get_footer(); ?>