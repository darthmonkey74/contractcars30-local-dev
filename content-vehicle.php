<?php
/**
 * @package progression
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="content-container-boxed vehicle-index">
		<a href="<?php the_permalink(); ?>">

			<?php if(has_post_thumbnail()): ?>
				<div class="vehicle-index-featured">
					<?php the_post_thumbnail('vehicle-index'); ?>
				</div>
			<?php else: ?>
			<div class="blank-image-index-vehicle"></div>
			<?php endif; ?>	
            <div class="corner_flash_1">
	     <?php //the_field('sales_flash'); ?>
	   <?php 
	   /*if(the_field('sales_flash') == "None"){
	      echo '<img src="'.get_template_directory_uri().'/images/FLASH-special-offer.png">';
	   } else if(the_field('sales_flash') == "Limited Stock"){
	      echo '<img src="'.get_template_directory_uri().'/images/FLASH-special-offer.png">';
	   } else if(the_field('sales_flash') == "Special Deal"){
	      echo '<img src="'.get_template_directory_uri().'/images/FLASH-special-offer.png">';
	   } else if(the_field('sales_flash') == "Special Offer"){
	      echo '<img src="'.get_template_directory_uri().'/images/FLASH-special-offer.png">';
	   }*/
	   
	   switch (get_field('sales_flash')) {
	   case "None":
          echo '';
         break;
     case "Limited Stock":
          echo '<img src="'.get_template_directory_uri().'/images/FLASH-limited-stock.png">';
         break;
     case "Special Deal":
          echo '<img src="'.get_template_directory_uri().'/images/FLASH-special-deal.png">';
         break;
     case "Special Offer":
         echo '<img src="'.get_template_directory_uri().'/images/FLASH-special-offer.png">';
         break;
}
	   ?>
	   </div>
       	<h3 class="vehicle-entry-title"><?php the_title(); ?></h3>
		</a>
		
		<div id="vehicle-price-index" >
			<?php echo do_shortcode('[vehicle_price]'); ?>
			<div class="clearfix"></div>
             <p>
                <a href="<?php the_permalink(); ?>#progression_contact" class="progression-button button-vehicle-index green"><?php _e( 'Get in Contact', 'progression' ); ?></a>
				<a href="<?php the_permalink(); ?>#alternative-payment-profiles" class="progression-button button-vehicle-index"><?php _e( 'View Deal', 'progression' ); ?></a>
        </p>
        <div class="clearfix"></div>
		</div>
		<!-- hide toggle car specs 
		<ul class="progression-toggle">
			<li class="progression_active"><i class="fa fa-cog"></i><?php _e( 'Specifications', 'progression' ); ?></li>
			<div class="div_progression_toggle">
				<?php //echo do_shortcode('[vehicle_specs]'); ?>
			<div class="clearfix"></div>
			</div>
		</ul>
        End hide toggle car specs  -->		
		<div class="clearfix"></div>
	</div><!-- close .content-container-boxed -->
</article><!-- #post-## -->
