<?php
// Template Name: Contact Page Full Width
/**
 *
 * @package progression
 * @since progression 1.0
 */

get_header(); ?>
<?php get_template_part( 'page', 'title' ); ?>

<div class="width-container">
	
	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'content', 'page' ); ?>
	<?php endwhile; // end of the loop. ?>
	
</div><!-- close .width-container -->
<?php get_footer(); ?>