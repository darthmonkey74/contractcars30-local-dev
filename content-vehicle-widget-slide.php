<?php
/**
 * @package progression
 */
?>

<div class="content-container-boxed vehicle-widget" style="padding:0px;">
<?php
//add_image_size('vehicle-index', 600, 150 );
//set_post_thumbnail_size( 400, 150, array( 'center', 'center'));
?>
	<a href="<?php the_permalink(); ?>">
		<?php if(has_post_thumbnail()): ?>
			<div class="vehicle-index">
				<?php
                    set_post_thumbnail_size( 400, 150, array( 'center', 'center'));
                 ?>				
			    
				<?php the_post_thumbnail('progression-slider-new');
				// the_post_thumbnail('vehicle-index-review-thumbnail'); ?>
				
				<div class="caption1"> 
                 <span><h3 style="color:#fff; font-weight:bold; text-align:left;padding: 0.5em 1em;">
				  <?php the_title(); ?>
				  </h3>
                  <br/>
				  <?php echo do_shortcode('[vehicle_price_banner]'); ?>
				  <br/>
				  <p id="vehicle-price-index" style="background: none; border-bottom: 0px; text-align:left; ">
		<a href="<?php the_permalink(); ?>#alternative-payment-profiles" class="progression-button button-vehicle-index" style="text-align:center;"><?php _e( 'View<br/>Offer', 'progression' ); ?></a>
        <a href="<?php the_permalink(); ?>#progression_contact" class="progression-button button-vehicle-index green" style="text-align:center;"><?php _e( 'Contact<br/>Us', 'progression' ); ?></a>
        </p>
				  </span>
				 
                  <!--<div class="caption2">
				     <p id="vehicle-price-index" style="background: none; border-bottom: 0px; text-align:left; ">
		<a href="<?php //the_permalink(); ?>#alternative-payment-profiles" class="progression-button button-vehicle-index" style="text-align:center;"><?php //_e( 'View<br/>Offer', 'progression' ); ?></a>
        <a href="<?php //the_permalink(); ?>#progression_contact" class="progression-button button-vehicle-index green" style="text-align:center;"><?php //_e( 'Contact<br/>Us', 'progression' ); ?></a>
        </p>
				  </div>-->
                </div>
				
			</div>
		<?php else: ?>
		<div class="blank-image-index-vehicle"></div>
		<?php endif; ?>
		
	  
        <!-- END Advanced custom field & corner flash HTML-->
		
	</a>
</div><!-- close .content-container-boxed -->