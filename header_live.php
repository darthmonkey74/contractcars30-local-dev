<?php
/**
 * The Header for our theme.
 *
 * @package progression
 * @since progression 1.0
 */
?><!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>  <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<!--[if IE]>
	<style>
       #car-search-submit {
       		padding: 1px!important;
       }
       	
}
    </style>
<![endif]-->

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
	<?php wp_head(); ?>
	
	 <!-- Insert to your webpage before the </head> -->
    <!--<script src="<?php //bloginfo('template_url'); ?>/carouselengine/jquery.js"></script>
    <script src="<?php //bloginfo('template_url'); ?>/carouselengine/amazingcarousel.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php //bloginfo('template_url'); ?>/carouselengine/initcarousel-1.css">
    <script src="<?php //bloginfo('template_url'); ?>/carouselengine/initcarousel-1.js"></script>-->
    <!-- End of head section HTML codes -->
	
</head>

<body <?php body_class(); ?>>
<header>
	<div class="width-container">
	<div class="header_left">
       <h1 id="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_theme_mod( 'logo_upload', get_template_directory_uri() . '/images/logo.png' ); ?>" alt="<?php bloginfo('name'); ?>" width="<?php echo get_theme_mod( 'logo_width', '155' ); ?>" /></a></h1>
		<div class="tagline"> <?php echo get_bloginfo ( 'description' );  ?></div>
        <!-- <div id="header-search">
			<?php // if (class_exists('Progression_Car_Dealer')): ?>
			<?php // echo do_shortcode('[vehicle_searchform include="keyword"]'); ?>
			<?php // else: ?>
			<?php // get_search_form(); ?>
			<?php//  endif; ?>
         -->
         </div>
             <div class="header_right">
             </div> 
		</div>
		<div class="clearfix"></div>
	</div><!-- close .width-container -->
	<nav>
		<div class="width-container">
			<?php wp_nav_menu( array('theme_location' => 'primary', 'depth' => 4, 'menu_class' => 'sf-menu') ); ?>
             <!-- <div class="social-ico">
                    <a target="_blank" href="https://www.facebook.com/contracthire"><i class="fa fa-facebook"></i></a>
                    <a target="_blank" href="https://twitter.com/contractcars"><i class="fa fa-twitter"></i></a>
                    <a target="_blank" href="https://plus.google.com/u/0/+Contractcarsuk/posts"><i class="fa fa-google-plus"></i></a>
                    <a target="_blank" href="https://www.linkedin.com/profile/view?id=141839851&amp;authType=NAME_SEARCH&amp;authToken=uXKl&amp;locale=en_US&amp;trk=tyah2&amp;trkInfo=idx%3A1-1-1%2CtarId%3A1417098147205%2Ctas%3Acontract+cars"><i class="fa fa-linkedin"></i></a>
                 </div> -->
		<div class="clearfix"></div>	
		</div><!-- close .width-container -->
	</nav>
</header>

<div id="main">